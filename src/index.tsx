import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import * as serviceWorkerRegistration from './serviceWorkerRegistration';
import reportWebVitals from "./reportWebVitals";
import PouchDB from "pouchdb";
import { Provider } from "use-pouchdb";
import PouchFind from 'pouchdb-find';

PouchDB.plugin(PouchFind);
var opts = { live: true, retry: false };
let db = new PouchDB("plantStats");
let remoteURL = process.env.REACT_APP_REMOTE_DB || "http://localhost:4000/plantstats";
const remoteDb = new PouchDB(remoteURL);
remoteDb.info().then(function(info) {
  console.log(info);
});
PouchDB.sync(remoteDb,db, opts);
PouchDB.plugin(require('pouchdb-find'));

ReactDOM.render(
  <Provider
    default="local"
    databases={{
      local: db,
      remote: remoteDb,
    }}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorkerRegistration.register();


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
