import { dataEntry, plantStat } from "../types/common";

export default function getValueTimelines(data: plantStat[]){

let moistureTimeline: dataEntry[] = [];

  data.map((entry) => {
    let currentEntry: dataEntry = {time: entry._id, value: entry.moisture}
    moistureTimeline.push(currentEntry);
  })
}
