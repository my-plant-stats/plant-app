import React from "react";
import { Line } from "@ant-design/charts";
import { dataEntry, sensorType } from "../types/common";
import { Card, Statistic } from "antd";
import "../styles/sparklineCharts.css";
import {
  RiCloudWindyLine,
  RiContrastDrop2Line,
  RiSunLine,
  RiTempColdLine,
} from "react-icons/ri";
import useValueStatus from "../hooks/useValueStatus";
import useTypeValue from "../hooks/useTypeValue";

const SparklineChart: React.FC<{
  type: sensorType;
  data: dataEntry[];
  loading: boolean;
  lastEntry: number;
  lastEntryDate: string;

}> = ({ type, data, loading, lastEntry, lastEntryDate}) => {

  // getting data

  let usedData: dataEntry[]  = useTypeValue(
    type
  );

  
  // Responsive Design

  let strokeColor = useValueStatus(type, lastEntry) || "#00A97F";
  let sensorSuffix;
  let iconType: React.ReactNode;

  switch (type) {
    case sensorType.moisture:
      iconType = <RiContrastDrop2Line className="sparkline-card-icon" />;
      sensorSuffix = "%";
      break;
    case sensorType.temperature:
      iconType = <RiTempColdLine className="sparkline-card-icon" />;
      sensorSuffix = "°C";
      break;
    case sensorType.lighting:
      iconType = <RiSunLine className="sparkline-card-icon" />;
      sensorSuffix = "lux";
      break;
    case sensorType.humidity:
      iconType = <RiCloudWindyLine className="sparkline-card-icon" />;
      sensorSuffix = "%";
      break;
  }

  // config for line g2plot

  const config = {
    data: usedData,
    height: 196,
    xField: "time",
    yField: "value",
    connectNulls: true,
    smooth: true,
    color: strokeColor,

    tooltip: {
      fields: ["value"],
      domStyles: {
        "g2-tooltip": {
          backgroundColor: "#141414",
          opacity: 0.75,
          boxShadow: "0 0 1em black",
        },
        "g2-tooltip-title": { color: "white" },
        "g2-tooltip-marker": { fill: strokeColor },
        "g2-tooltip-list": { color: "white" },
        "g2-tooltip-list-item": { color: "white" },
      },
      marker: {
        style: {
          markerColor: strokeColor,
        },
      },
    },

    slider: {
      backgroundStyle: { opacity: 0 },
      foregroundStyle: { opacity: 0.1, fill: "white" },
      textStyle: { opacity: 0 },
      handlerStyle: {
        hight: 30,
        radius: 2,
        fill: "#111F1C",
        highLightFill: strokeColor,
      },
      trendCfg: {
        areaStyle: { backgroundColor: "#141414" },
      },
      cursor: "pointer",
    },

    grid: {
      line: {
        style: {
          strokeOpacity: 0.75,
        },
      },
    },

    lineStyle: {
      r: 5,
      stroke: strokeColor,
      lineWidth: 3,
      strokeOpacity: 0.7,
      shadowColor: "black",
      cursor: "pointer",
    },
  };
  
  if (data) {
    return (
      <>
        <Card
          headStyle={{ opacity: 1 }}
          title={
            <h2 className="sparkline-card-title">
              {iconType} {type}
            </h2>
          }
          extra={
            <Statistic
              title={"last: " + lastEntryDate}
              //value={lastEntry?.[1]}
              value={lastEntry}
              suffix={sensorSuffix}
              valueStyle={{ color: strokeColor }}
            ></Statistic>
          }
        >
          <Line loading={loading} {...config} />
        </Card>
      </>
    );
  } else {
    return <></>;
  }
};

export default SparklineChart;
