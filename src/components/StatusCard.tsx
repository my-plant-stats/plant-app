import { SyncOutlined } from "@ant-design/icons";
import { Card, Statistic } from "antd";
import React from "react";
import {
  RiAlertFill,
  RiCloudWindyLine,
  RiContrastDrop2Line,
  RiSunLine,
  RiTempColdLine,
} from "react-icons/ri";
import UpdateCycle from "../components/UpdateCycle";
import useValueStatus from "../hooks/useValueStatus";
import "../styles/StatusCard.css";
import { sensorType, statusColor } from "../types/common";

const StatusCard: React.FC<{
  lastMoisture: number;
  lastTemperature: number;
  lastLighting: number;
  lastHumidity: number;
  lastEntryDate: string;
}> = ({
  lastMoisture,
  lastTemperature,
  lastLighting,
  lastHumidity,
  lastEntryDate,
}) => {
  
  // generating status + color from interface and lastValues

  let moistureStatusColor =
    useValueStatus(sensorType.moisture, lastMoisture) || "#00A97F";
  //console.log(moistureStatusColor);
  let temperatureStatusColor =
    useValueStatus(sensorType.temperature, lastTemperature) || "#00A97F";
  let lightingStatusColor =
    useValueStatus(sensorType.lighting, lastLighting) || "#00A97F";
  let humidityStatusColor =
    useValueStatus(sensorType.humidity, lastLighting) || "#00A97F";
  let usedStatusColor = "";
  let statusBarColor = "#00A97F";

  let statusIcon: React.ReactNode = <SyncOutlined />;
  let statusText: string = "Everything is allright.";

  if (moistureStatusColor === statusColor.alert) {
    usedStatusColor = statusColor.alert;
    statusBarColor = statusColor.alert;
    statusIcon = <RiAlertFill />;
    statusText = "Soil Moisture level decreasing!";
  }
  if (moistureStatusColor === statusColor.fatal) {
    usedStatusColor = statusColor.fatal;
    statusBarColor = statusColor.fatal;
    statusIcon = <RiAlertFill />;
    statusText = "Soil Moisture level low!";
  }

  if (temperatureStatusColor === statusColor.alert) {
    usedStatusColor = statusColor.alert;
    statusBarColor = statusColor.alert;
    statusIcon = <RiAlertFill className="icon" />;
    statusText = "Temperature above 28°C!";
  }

  if (temperatureStatusColor === statusColor.fatal) {
    usedStatusColor = statusColor.fatal;
    statusBarColor = statusColor.fatal;
    statusIcon = <RiAlertFill className="icon" />;
    statusText = "Plant is getting dangerously hot/cold!";
  }

  // building status node

  let status: React.ReactNode = (
    <div style={{ color: usedStatusColor }}>
      {statusIcon} {statusText}
    </div>
  );

  let title: React.ReactNode = (
    <div className="title">
      {status}
      <div className="progress-bar">
        <UpdateCycle color={statusBarColor} />
      </div>
    </div>
  );

  //  grid style options
  const gridStyle = {
    width: "25%",
  };

  return (
    <Card
      title={title}
      extra={<div className="title-extra">last: {lastEntryDate}</div>}
    >
      <Card.Grid style={gridStyle} hoverable={false}>
        <Statistic
          title={<RiContrastDrop2Line />}
          value={Number(lastMoisture).toFixed(1)}
          suffix={"%"}
          valueStyle={{ color: moistureStatusColor }}
        ></Statistic>
      </Card.Grid>
      <Card.Grid style={gridStyle} hoverable={false}>
        <Statistic
          title={<RiTempColdLine />}
          value={Number(lastTemperature).toFixed(1)}
          suffix={"°C"}
          valueStyle={{ color: temperatureStatusColor }}
        ></Statistic>
      </Card.Grid>
      <Card.Grid style={gridStyle} hoverable={false}>
        <Statistic
          title={<RiSunLine />}
          value={Number(lastLighting).toFixed(1)}
          suffix={"lux"}
          valueStyle={{ color: lightingStatusColor }}
        ></Statistic>
      </Card.Grid>
      <Card.Grid style={gridStyle} hoverable={false}>
        <Statistic
          title={<RiCloudWindyLine />}
          value={Number(lastHumidity).toFixed(1)}
          suffix={"%"}
          valueStyle={{ color: humidityStatusColor }}
        ></Statistic>
      </Card.Grid>
    </Card>
  );
};

export default StatusCard;
