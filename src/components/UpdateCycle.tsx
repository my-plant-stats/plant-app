import React, { useEffect, useState } from "react";
import { Progress } from "antd";

const UpdateCycle: React.FC<{ color: string }> = ({ color }) => {
  let progressSeconds: number;
  const [value, setValue] = useState(0);

  useEffect(() => {
    setInterval(() => {
      getPercent();
    }, 1000);
  },[value]);

  // calculating progress over a h hour intervall
  function getPercent() {
    const currentTime = new Date();
    if(currentTime.getHours() < 6 ){
      progressSeconds = (currentTime.getHours()*60*60) + (currentTime.getMinutes() * 60) + currentTime.getSeconds();
    }
    if(currentTime.getHours() >= 6  && currentTime.getHours() < 12){
      progressSeconds = ((currentTime.getHours()-6)*60*60) + (currentTime.getMinutes() * 60) + currentTime.getSeconds();
    }
    if(currentTime.getHours() >= 12  && currentTime.getHours() < 18){
      progressSeconds = ((currentTime.getHours()-12)*60*60) + (currentTime.getMinutes() * 60) + currentTime.getSeconds();
    }
    if(currentTime.getHours() >= 18  && currentTime.getHours() < 24){
      progressSeconds = ((currentTime.getHours()-18)*60*60) + (currentTime.getMinutes() * 60) + currentTime.getSeconds();
    } 
    if(currentTime.getHours() % 6 === 0 && currentTime.getMinutes() === 0 && currentTime.getSeconds() === 0) {
      progressSeconds = (6 * 60 * 60);
    }
    setValue((progressSeconds / (6 * 60 * 60)) * 100);
  }

  return (
    <Progress
      strokeColor={{
        from: color,
        to: color,
      }}
      percent={value}
      status="active"
      showInfo={false}
      strokeLinecap="round"
    />
  );
};

export default UpdateCycle;
