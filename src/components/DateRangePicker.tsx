import {  useContext } from "react";
import { DatePicker } from "antd";
import { RangePickerProps } from "antd/lib/date-picker";
import { RangeContext } from "../App";

const DateRangePicker: React.FC<RangePickerProps> = ({
  onCalendarChange,
  defaultValue,
}) => {
  const { RangePicker } = DatePicker;
  const context = useContext(RangeContext)

  return (
    <>
      <RangePicker
        size="middle"
        defaultValue={defaultValue}
        style={{ maxWidth: "300px" }}
        onCalendarChange={(range, formalString, info) => {
          context.setRange(range);
          if (onCalendarChange) {
            onCalendarChange(range, formalString, info);
          }
        }}
      />
    </>
  );
};

export default DateRangePicker;
