export interface plantStat {
  _id: string;
  moisture: number;
  temperature: number;
  lighting: number;
  humidity: number;
}

export interface dataEntry {
  time: string;
  value: Number;
}

export enum sensorType {
  moisture="Soil Moisture",
  temperature="Temperature",
  lighting="Lighting",
  humidity="Humidity",
}

export enum statusColor {
  normal="#00A97F",
  alert="#fa8c16",
  fatal="#f5222d",
}

