import React, { createContext, useReducer } from "react";

const initialState = {
  isFetching: false,
  plantStats: [],
};

const Reducer = (state: any, action: { type: any; payload: any; }) => {
  switch (action.type) {
    case "FETCH_PLANTSTATS_START":
      console.log("Fetching PlantStats");
      return { ...state, isFetching: true };
    case "FETCH_PLANTSTATS_SUCCESS":
      console.log(action.payload);
      return { ...state, isFetching: false, questions: action.payload };
    case "FETCH_PLANTSTATS_FAILURE":
      const error = action.payload;
      alert(
        `There was an error fetching PlantStats: ${error.message}. Please try again.`
      );
      return { ...state, isFetching: false };
    default:
      throw new Error();
  }
};

export const Context = createContext(initialState)

// export const StoreProvider = ({children}) => {
//   const [state, dispatch] = useReducer(Reducer, initialState);

//   return(
//     <Context.Provider value={[state, dispatch]}>
//       { children }
//     </Context.Provider>
//   )
// }
