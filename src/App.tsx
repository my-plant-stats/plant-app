import { Divider, BackTop, PageHeader, Row, Col } from "antd";
import {
  RiCopyrightLine,
  RiHeart2Line,
  RiPlantFill,
} from "react-icons/ri";
import { Content, Footer, Header } from "antd/lib/layout/layout";
import React, { createContext, useState } from "react";
import "./styles/css/antd.dark.css";
import "./styles/App.css";
import SparklineChart from "./components/SparklineChart";
import DateRangePicker from "./components/DateRangePicker";
import { sensorType } from "./types/common";
import moment from "moment";
import { RangeValue } from "rc-picker/lib/interface";
import StatusCard from "./components/StatusCard";
import useLastData from "./hooks/useLastData";

let defaultRange: RangeValue<moment.Moment> = [
  moment().subtract(1, "week"),
  moment(),
];

let isLoading = false;
const AppContext = React.createContext({});

const defaultRangeContext = {range: defaultRange as RangeValue<moment.Moment> , setRange:(value: RangeValue<moment.Moment>) =>{defaultRange = value} };


export const RangeContext = createContext(defaultRangeContext)

const App: React.FC = () => {
  let lastData = useLastData();
  const [newRange, setRange] = useState<RangeValue<moment.Moment>>(defaultRange);

  // init data

  const currentYear = new Date().getFullYear();
  const lastMoisture = lastData?.moisture;
  const lastTemperature = lastData?.temperature;
  const lastLighting = lastData?.lighting;
  const lastHumidity = lastData?.humidity;
  const lastEntryDate = moment(lastData?._id).format("YYYY-MM-DD HH:mm");

  return (
    <AppContext.Provider value={{ dbName: 'plantStats' }}>
      <RangeContext.Provider value={{range: newRange, setRange}}>
      <BackTop />
      <Header>
        <PageHeader
          className="site-page-header"
          title={
            <h4 className="header-title">
              <RiPlantFill className="header-icon" /><p className="header-title-font"> PlantApp</p>
            </h4>
          }
          extra={[
            <DateRangePicker
              onCalendarChange={setRange}
               defaultValue={defaultRange}
            />,
          ]}
        />
      </Header>
      <Content>
        <Row>
          <Col span={22} offset={1}>
            <Divider dashed />
            <StatusCard
              lastEntryDate={lastEntryDate}
              lastMoisture={lastMoisture}
              lastTemperature={lastTemperature}
              lastLighting={lastLighting}
              lastHumidity={lastHumidity}
            />
             <Divider dashed />
            <SparklineChart
              type={sensorType.moisture}
              data={[]}
              loading={isLoading}
              lastEntry={lastMoisture}
              lastEntryDate={lastEntryDate}
            />
            <Divider dashed />
            <SparklineChart
              type={sensorType.temperature}
              data={[]}
              loading={isLoading}
              lastEntry={lastTemperature}
              lastEntryDate={lastEntryDate}
            />
            <Divider dashed />
            <SparklineChart
              type={sensorType.lighting}
              data={[]}
              loading={isLoading}
              lastEntry={lastLighting}
              lastEntryDate={lastEntryDate}
            />
            <Divider dashed />
            <SparklineChart
              type={sensorType.humidity}
              data={[]}
              loading={isLoading}
              lastEntry={lastHumidity}
              lastEntryDate={lastEntryDate}
            />
            <Divider dashed />
            {/* <Skeleton active /> */}
          </Col>
        </Row>
      </Content>
      <Footer>
        <Row>
          <Col span={22} offset={1}>
            <div className="footer">
              <div>
                {" -- "}Made with <RiHeart2Line className="icon-love"/>
                {" -- "}
              </div>
            </div>
          </Col>
          <Col span={22} offset={1}>
            <div className="footer">
              <div>
                {" -- "}
                <RiCopyrightLine /> {currentYear} Tobias Potye{" -- "}
              </div>
            </div>
          </Col>
        </Row>
      </Footer>
      </RangeContext.Provider>
      </AppContext.Provider>
  );
};

export default App;
