import { useAllDocs } from 'use-pouchdb'
import { plantStat } from '../types/common'
import { RangeValue } from "rc-picker/lib/interface";
import moment from 'moment';

export default function useAllData(range: RangeValue<moment.Moment>) {
  
  const rangeBegin = moment(range?.[0]).toISOString();
  const rangeEnd = moment(range?.[1]).toISOString();
  let result: plantStat[] = [];
  const { rows, state, loading, error } =   useAllDocs<plantStat>({
    startkey: rangeEnd,
    endkey: rangeBegin,
    include_docs: true,
    descending: true,
    //limit: ,
  })

  rows.map(({ id, doc }) => {
    let stat:plantStat =  {_id: id!, moisture: doc?.moisture!, temperature: doc?.temperature!, lighting: doc?.lighting!, humidity: doc?.humidity!};
    result.push(stat)
  })

  if (loading && rows.length === 0) {
    return //<Loading />
  }
  
  if (state === 'error' && error) {
    return //<Error error={error} />
  }

  return result
}