import { sensorType, statusColor } from "../types/common";

export default function useValueStatus(type: sensorType, lastValue: number) {
  let status;

  switch (type) {

    // moisture status
    case sensorType.moisture:
      if (lastValue >= 75) {
        status = statusColor.normal;
      }
      if (lastValue < 75) {
        status = statusColor.alert;
      }
      if (lastValue < 45) {
        status = statusColor.fatal;
      }
      break;

    // temperature status
    case sensorType.temperature:
      if (lastValue > 40 || lastValue < 10) {
        status = statusColor.fatal;
      }
      if (lastValue >= 28 || lastValue < 15) {
        status = statusColor.alert;
      }
      if (lastValue < 28) {
        status = statusColor.normal;
      }
      break;

    // lighting status
    case sensorType.lighting:
      if (type === sensorType.lighting) {
        status = statusColor.normal;
      }
      break;
      
    // humidity status
    case sensorType.humidity:
      if (type === sensorType.humidity) {
        if (lastValue > 90 || lastValue < 20) {
          status = statusColor.fatal;
        }
        if (lastValue > 70 || lastValue < 30) {
          status = statusColor.alert;
        }
        status = statusColor.normal;
      }
      break;
  }

  return status;
}
