import moment from "moment";
import { useFind } from "use-pouchdb";
import { plantStat } from "../types/common";

export default function useLastData() {
  const { docs} = useFind<plantStat>({
    // Ensure that this index exist, create it if not. And use it.

    index: {
      fields: ["_id"],
      // 'ddoc' and 'name' are not required. PouchDB will check all existing indexes
      // if they match the requirements. And only create a one if none match.
    },
    selector: {
      //_id: {$gt: "_id" }
      //type: "json",
    },
    sort: [{ _id: "desc" }],
    //sort: ['_id'],
    fields: ["_id", "moisture", "temperature", "humidity", "lighting"],
    limit: 1,
  });

  //console.log(docs);
  if(docs.length === 0){
    const currentDate= new Date();
    const demoDoc: plantStat = {
      _id: moment(currentDate).format("YYYY-MM-DD HH:mm"),
  moisture: Number(getRandomInt(75,95).toFixed(1)),
  temperature: Number(getRandomInt(20,28).toFixed(1)),
  lighting: Number(getRandomInt(400,1000).toFixed(1)),
  humidity: Number(getRandomInt(45,60).toFixed(1)),
    }
    return demoDoc;
  }
  return docs[0];
}

function getRandomInt(min: number, max: number) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}