import { useFind } from "use-pouchdb";
import { dataEntry, sensorType } from "../types/common";
import moment from "moment";
import { values } from "@antv/util";
import { useContext } from "react";
import { RangeContext } from "../App";

export default function useTypeValue(type: sensorType) {
  const range = useContext(RangeContext).range;
  const rangeBegin = moment(range?.[0]).toISOString();
  const rangeEnd = moment(range?.[1]).toISOString();
  let valueType: string;
  let entriesPerDay = 4;
  if (type === sensorType.moisture) {
    valueType = "moisture";
  } else {
    valueType = type.toLocaleLowerCase();
  }
  const dayCount =
    Number(
      moment
        .duration(
          moment(rangeEnd, "YYYY-MM-DD").diff(moment(rangeBegin, "YYYY-MM-DD"))
        )
        .asDays()
        .toFixed(1)
    ) + 1;
  let workingData: dataEntry[] = [];

  const { docs, state, error } = useFind<dataEntry[]>({
    // Ensure that this index exist, create it if not. And use it.

    index: {
      fields: ["_id"],
      // 'ddoc' and 'name' are not required. PouchDB will check all existing indexes
      // if they match the requirements. And only create a one if none match.
    },
    selector: {
      _id: { $gt: rangeBegin, $lte: rangeEnd },
      //type: "json",
    },
    sort: ["_id"],
    fields: ["_id", valueType],
  });

  // adding all dataEntry to array to work with
  docs.map((entry) =>
    workingData.push({
      time: moment(entry._id).format("YYYY-MM-DD HH:mm"),
      value: Number(values(entry)[1]),
    })
  );
  workingData.reverse();

  // adding only entriesPerDay dataEntries per day to data
  let data: dataEntry[] = [];
  let workingDay: dataEntry[] = [];

  for (let i = 0; i < workingData.length; i++) {
    if (i === 0) {
      workingDay = [];
      workingDay.push(workingData?.[i]);
    } else {
      if (
        moment(workingData[i]?.time).date() ===
        moment(workingData[i - 1]?.time).date()
      ) {
        workingDay.push(workingData?.[i]);
      } else {
        let totalEntries = workingDay.length;
        let workingAmount = Math.ceil(totalEntries / entriesPerDay);
        for (let c = 0; c < workingDay.length; c++) {
          if (c % workingAmount === 0) data.push(workingDay?.[c]);
        }
        workingDay = [];
      }
    }
  }  
   
  data.reverse();
  if (data.length === 0){
    return demoData(rangeEnd, dayCount);
  } else {
    return data;
  }
}

// demoData while loading / or no connection to db 
function demoData(inputMoment: string, dayCount: Number) {
  const currentDate = moment(inputMoment);
  const data: dataEntry[] = [];

  for (let day = 0; day < dayCount; day++) {
    let currentDay = moment(currentDate, "YYYY-MM-DD").add(-day, "day");
    for (let entry = 0; entry < 24; entry = entry + 6) {
      let currentHour = moment(currentDay, "YYYY-MM-DD HH:mm");
      currentHour.set({hour: Math.abs(entry-24), minute:0, second:0, millisecond:0});
      data.push({
        time: moment(currentHour).format("YYYY-MM-DD HH:mm"),
        value: Number(Math.floor(Math.random()* 5) +1+ (Math.abs(entry-24)/2)),
      });
    }
  }
  data.reverse();
  return data;
}
