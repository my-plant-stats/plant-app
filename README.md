# Getting Started with Plant-App

setting up `.env` with `REACT_APP_REMOTE_DB=your_url/plantStats` with your URL to a `pouchdb-server` the default value is "`http://localhost:4000/plantStats"`


## Available Scripts

In the project directory, you can run:

### `npm create-theme`

Runs only `lessc` to compile new antd.css after adding changes to `dark-theme.less` / `light-theme.less` files.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm run build`

to build. use build to deploy on any static webserver eg enginx

## Deploy finished prebuild

or just install the latest finished `build` on a static webserver (need plant-db on same device or REACT_APP_REMOTE_DB=url setup in .env before build)

![](/screenshot.png)

